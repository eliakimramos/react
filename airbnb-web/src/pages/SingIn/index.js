import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import Logo from "../../assets/airbnb-logo.svg";
import api from "../../services/api";
import { login } from "../../services/auth";

import { Form, Container } from "./styles";

class SingIn extends Component {
    state = {
        email     : "",
        password  : "",
        error     : ""
    };

    handleSingIn = async e => {
        e.preventDefault();
        const {email,password} = this.state;
        if(!email || !password){
            this.setState({error: "Peencha e-mail e senha para continuar!"});
        }else{
            try{
                const response = await api.post("/sessions", {email, password});
                login(response.data.token);
                this.props.history.push("/app");
            }catch(err){
                this.setState({
                    error:
                    "Houve um problema com o login, verifique suas credenciais. T.T"
                });
            }
        }
    };

    render(){
        return (
            <Container>
                <Form onSubmit={this.handleSingIn}>
                    <img src={Logo} alt="Airbnb Logo" />
                    {this.state.error && <p>{this.state.error}</p>}
                    <input type="email" placeholder="Endereço de e-mail" onChange={e => this.setState({email: e.target.value})}/>
                    <input type="password" placeholder="senha" onChange={e => this.setState({password: e.target.value})} />
                    <button type="submit">Entrar</button>
                    <hr />
                    <Link to="/singup">Criar conta grátis</Link>
                </Form>
            </Container>
        );
    }
}

export default withRouter(SingIn);